# CPSC 501 - Assignment 2
## Object Inspector 
This tool allows you to inspect the following properties of an Object:

 - Class 
    - Immediate superclass
    - Implemented interfaces
 - Fields
    - Modifiers
    - Name
    - Current value (if a primitive)
    - Non-Recursive mode: class name and reference hash for non-primitive fields
    - Recursive mode: Inspection of non-primitive Fields
 - Constructors
    - Modifiers
    - Parameter types
 - Methods (and inherited Methods)
    - Modifiers
    - Return type
    - Name
    - Parameter types
    - Exceptions thrown